package ru.omen.app.pages;

import lib.Init;
import org.junit.Assert;
import org.openqa.selenium.*;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by OmeN on 26.05.2016.
 */
public class AnyPage {

    public AnyPage() {
        PageFactory.initElements(Init.getDriver(), this);
        waitPageToLoad();
    }

    private void waitPageToLoad() {
        new WebDriverWait(Init.getDriver(), 30).until((ExpectedCondition<Boolean>) wd -> ((JavascriptExecutor) wd)
                .executeScript("return document.readyState").equals("complete"));
        System.out.println("Page Is loaded!"); // Allure.fire
    }

    public void click(WebElement element) {
        new WebDriverWait(Init.getDriver(), 30).until(ExpectedConditions.elementToBeClickable(element));
        element.click();
        System.out.println("\t" + element.getTagName() + " click!"); // Allure.fire
    }

    public void click(By by) {
        click(getElement(by));
    }

    public WebElement getElement(By by) {
        return new WebDriverWait(Init.getDriver(), 30).until(ExpectedConditions.presenceOfElementLocated(by));
    }

    public WebElement getElement(String xpath) {
        return getElement(By.xpath(xpath));
    }

    public void selectItem(WebElement element, ArrayList<String> values) {
        if(values.get(0).isEmpty())
            return;
        Select select = new Select(element);
        for (WebElement elm : select.getOptions()) {
            for (String v : values) {
                if (elm.getText().toLowerCase().contains(v)) {
                    elm.click();
                    System.out.println("\t"+elm.getText()); // Allure.fire
                    break;
                }
            }
        }
    }

    public void selectItem(WebElement element, String value) {
        Select select = new Select(element);
        for (WebElement elm : select.getOptions()) {
            if (elm.getText().toLowerCase().contains(value)) {
                elm.click();
                System.out.println("\t"+elm.getText()); // Allure.fire
                break;
            }
        }
    }

    public void checkBox(WebElement element, Boolean value) throws InterruptedException {
        if (element.isDisplayed()) {
            if (element.isSelected() != value) {
                click(element);
                Assert.assertEquals("Неверное значение чекбокса, небходимо: " + value + ", элемент: " + element.isSelected(),
                        element.isSelected(), value);
                System.out.println("\telement setCheckbox"); // Allure.fire
            }
        } else {
            System.out.println("\tЭлемент не отображен на странице"); // Allure.fire
        }
    }

    public void setText(WebElement element, Object text) {
        element.clear();
        element.sendKeys(text.toString());
        System.out.println("\t"+element.getAttribute("value")); // Allure.fire
    }

    public void selectRadioBtn(List<WebElement> listBtn, String value) {
        for(WebElement element : listBtn) {
            String textBtn = element.getText().toLowerCase().trim();
            if(textBtn.contains(value)) {
                element.click();
                break;
            }
        }

    }
}
