package ru.omen.app.pages;

import lib.Init;
import lib.SearchSettings;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by OmeN on 26.05.2016.
 */
public class DromPage extends AnyPage {

    @FindBy(xpath = "//li[contains(.,'Автомобили')]")
    private WebElement autoURL; // Раздел "Автомобили"

    @FindBy(id = "searchLink")
    private WebElement addSearch;

    @FindBy(id = "fid")
    private WebElement selectFirma; // фирма

    @FindBy(id = "mid")
    private WebElement selectModel; // модель

    @FindBy(id = "minprice")
    private WebElement inputMinPrice; // минимальная цена

    @FindBy(id = "maxprice")
    private WebElement inputMaxPrice; // максмальная цена

    @FindBy(id = "minyear")
    private WebElement selectMinYear; // минимальный год

    @FindBy(id = "maxyear")
    private WebElement selectMaxYear; // максимальный год

    @FindBy(id = "mv")
    private WebElement selectEngineCapacityWith; // объем двигателя начальный

    @FindBy(id = "xv")
    private WebElement selectEngineCapacityOn; // объем двигателя конечный

    @FindBy(id = "fueltype")
    private WebElement selectFuelType; // тип топлива

    @FindBy(id = "transmission")
    private WebElement selectTransmission; // КПП

    @FindBy(id = "privod")
    private WebElement selectPrivod; // Привод

    @FindBy(id = "ph")
    private WebElement checkboxPhoto; // С фото

    @FindBy(id = "wl")
    private WebElement checkboxLeftWheel; // Левый руль

    @FindBy(id = "inomarka")
    private WebElement checkboxInomarka; // Иномарка

    @FindBy(xpath = "//div[contains(@class,'color-choose')]//a")
    private List<WebElement> listColor; // Цвета

    @FindBy(xpath = "//input[@name='pts']//..//..//label")
    private List<WebElement> listPts; // ПТС

    @FindBy(xpath = "//input[@name='damaged']//..//..//label")
    private List<WebElement> listState;

    @FindBy(xpath = "//div[contains(@class,'b-carBodies')]//label//div[2]")
    private List<WebElement> listCarBodies;

    @FindBy(id = "trade_ok")
    private WebElement checkboxTrade;

    @FindBy(xpath = "//div[span[contains(.,'Дополнительно')]]//label")
    private List<WebElement> listOtherSett;

    @FindBy(xpath = "//div[contains(@class, 'trade_view')]//label")
    private List<WebElement> listTradeSett;

    @FindBy(className = "ok")
    private WebElement btnSearch;

    private static final String xpathHrefAuto = "//li[contains(.,'Автомобили')]";

    public static final String SETTING_FIRMA = "фирма"; //
    public static final String SETTING_MODEL = "модель"; //
    public static final String SETTING_PRICE = "цена"; //
    public static final String SETTING_YEAR = "год"; //
    public static final String SETTING_ENGINE = "объем";
    public static final String SETTING_FUEL = "топливо"; //
    public static final String SETTING_TRANSMISSION = "кпп"; //
    public static final String SETTING_PRIVOD = "привод"; //
    public static final String SETTING_PHOTO = "с фото";
    public static final String SETTING_WL = "левый руль"; //
    public static final String SETTING_INOMARKA = "иномарка";
    public static final String SETTING_COLOR = "цвет"; //
    public static final String SETTING_PTS = "наличие птс";
    public static final String SETTING_STATE = "состояние";
    public static final String SETTING_BODIES = "тип кузова";
    public static final String SETTING_TRADE = "обмен"; //
    public static final String SETTING_OTHER = "дополнительно";

    private static SearchSettings settings;


    public DromPage() {
        new WebDriverWait(Init.getDriver(), 30)
                .until(ExpectedConditions
                        .presenceOfElementLocated(By.xpath(xpathHrefAuto)));

        System.out.println("url open_OK"); // Allure.fire
    }

    public void tabAuto() {
        click(autoURL);
        System.out.println("click_OK!"); // Allure.fire
    }

    public void setupSettings() throws InterruptedException {
        settings = new SearchSettings();
        addSearch.click();
        try {
            selectItem(selectFirma, settings.get(SETTING_FIRMA));
            selectItem(selectModel, settings.get(SETTING_MODEL));
            setPrice();
            setYear();
            setEngine();
            selectItem(selectFuelType, settings.get(SETTING_FUEL));
            selectItem(selectTransmission, settings.get(SETTING_TRANSMISSION));
            selectItem(selectPrivod, settings.get(SETTING_PRIVOD));
            checkBox(checkboxPhoto, !settings.get(SETTING_PHOTO).get(0).isEmpty());
            checkBox(checkboxLeftWheel, !settings.get(SETTING_WL).get(0).isEmpty());
            checkBox(checkboxInomarka, !settings.get(SETTING_INOMARKA).get(0).isEmpty());
            selectColors();
            selectRadioBtn(listPts, settings.get(SETTING_PTS).get(0));
            selectRadioBtn(listState, settings.get(SETTING_STATE).get(0));
            selectMulty(listCarBodies, SETTING_BODIES);
            selectTrade();
            selectMulty(listOtherSett, SETTING_OTHER);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setYear() {
        if(!settings.get(SETTING_YEAR).get(0).isEmpty()) {
            selectItem(selectMinYear, settings.get(SETTING_YEAR).get(0));
            selectItem(selectMaxYear, settings.get(SETTING_YEAR).get(1));
        }
    }

    public void setEngine() {
        if(!settings.get(SETTING_ENGINE).get(0).isEmpty()) {
            selectItem(selectEngineCapacityWith, settings.get(SETTING_ENGINE).get(0));
            selectItem(selectEngineCapacityOn, settings.get(SETTING_ENGINE).get(1));
        }
    }
    public void setPrice() {
        if(!settings.get(SETTING_PRICE).get(0).isEmpty()) {
            setText(inputMinPrice, settings.get(SETTING_PRICE).get(0));
            setText(inputMaxPrice, settings.get(SETTING_PRICE).get(1));
        }
    }

    public void selectColors() {
        if(settings.get(SETTING_COLOR).get(0).isEmpty())
            return;
        for(WebElement element : listColor) {
            String colorLink = element.getAttribute("title").toLowerCase().trim();
            ArrayList<String> colorSett = settings.get(SETTING_COLOR);
            if(colorSett.contains(colorLink)) {
                element.click();
            }
        }
    }

    public void selectTrade() throws InterruptedException {
        if(!settings.get(SETTING_TRADE).get(0).isEmpty()) {
            checkBox(checkboxTrade, true);
            selectMulty(listTradeSett, SETTING_TRADE);
        }

    }

    public void selectMulty(List<WebElement> list, String Setting) {
        if(settings.get(Setting).get(0).isEmpty())
            return;

        for(WebElement element : list) {
            String link = element.getText().toLowerCase().trim();
            ArrayList<String> sett = settings.get(Setting);
            if(sett.contains(link)) {
                element.click();
                System.out.println("click" + element.getAttribute("id"));
            }
        }
    }

    public void clickSearch() {
        btnSearch.submit();
    }
}
