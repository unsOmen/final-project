package ru.omen.app.pages;

import lib.Init;
import lib.SearchSettings;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.junit.Assert;
import org.openqa.selenium.*;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import ru.yandex.qatools.allure.Allure;
import ru.yandex.qatools.allure.annotations.Attachment;
import ru.yandex.qatools.allure.events.AddParameterEvent;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by OmeN on 28.05.2016.
 */
public class ResultPage extends AnyPage {

    private static final String XPATH_RESULT_TABLE = "(//table[contains(@class, 'newCatList')])[1]";

    @FindBy(xpath = "//div[contains(@class,'pager')]//div//a")
    private List<WebElement> listPages;

    @FindBy(xpath = "(//table[contains(@class, 'newCatList')])[1]//tr[@class='row']//nobr//a")
    private List<WebElement> listResult;

    private float last_best_price;
    private int last_best_year;
    private String last_best_uri;
    private long last_best_number;

    private static long carNumber = 1;

    public ResultPage() {
        new WebDriverWait(Init.getDriver(), 30)
                .until(ExpectedConditions
                        .presenceOfElementLocated(By.xpath(XPATH_RESULT_TABLE)));

        System.out.println("RESULT_OK"); // Allure.fire

    }

    public void parseResult() {
        try {
            for (WebElement element : listResult) {
                Document doc = Jsoup.connect(element.getAttribute("href")).get();
                checkSettings(doc);
            }
            log("Текущий лучший результат (" + last_best_number + ")", last_best_uri);
            if (listPages.size() > 0) {
                for (WebElement elm : listPages) {
                    System.out.println(elm.getAttribute("href"));
                    Document nextPage = Jsoup.connect(elm.getAttribute("href")).get();
                    Elements cars = nextPage.select("table[class*=newCatList] tr[class$=row] nobr a");
                    log("След. страница", nextPage.baseUri());
                    for (Element car : cars) {
                        Document newDoc = Jsoup.connect(car.attr("href")).get();
                        checkSettings(newDoc);
                        System.out.println(newDoc.attr("href"));
                    }
                    log("Текущий лучший результат (" + last_best_number + ")", last_best_uri);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("best: " + last_best_price + " - " + last_best_year + " - " + last_best_uri);
        log("Итоговый лучший результат (" + last_best_number + ")", last_best_uri);
    }




    public void openBestResult() {
        Init.getDriver().findElement(By.cssSelector("body")).sendKeys(Keys.CONTROL + "t");
        Init.getDriver().navigate().to(last_best_uri);
    }

    private void checkSettings(Document doc) {
        String curr_uri = doc.baseUri();
        String curr_name = "";
        float curr_price = 0;
        int curr_year = 0;

        Element elm = doc.select("td[class=adv-text] > h3").first();
        String[] values = elm.text().toLowerCase().trim().replace(",", "").split(" ");
        curr_name = values[0] + " " + values[1];
        curr_year = new Integer(values[2]);

        if (!SearchSettings.get(DromPage.SETTING_FIRMA).get(0).isEmpty())
            Assert.assertTrue("Разная фирма!", SearchSettings.checkSetting(DromPage.SETTING_FIRMA, values[0], SearchSettings.Type.STR));
        if (!SearchSettings.get(DromPage.SETTING_MODEL).get(0).isEmpty())
            Assert.assertTrue("Разная марка!", SearchSettings.checkSetting(DromPage.SETTING_MODEL, values[1], SearchSettings.Type.STR));
        if (!SearchSettings.get(DromPage.SETTING_YEAR).get(0).isEmpty())
            Assert.assertTrue("Неверный год", SearchSettings.checkSetting(DromPage.SETTING_YEAR, values[2], SearchSettings.Type.INT));

        elm = doc.select("td[class=adv-text] > div[class$=b-media-cont_theme_dot]").first();
        values = elm.text().toLowerCase().trim().split("руб.");
        values[0] = values[0].replace(values[0].charAt(values[0].length() - 1), ' ').replace(" ", "");
        curr_price = new Float(values[0]);
        if (!SearchSettings.get(DromPage.SETTING_PRICE).get(0).isEmpty())
            Assert.assertTrue("Цена не совпадает!", SearchSettings.checkSetting(DromPage.SETTING_PRICE, values[0], SearchSettings.Type.INT));

        elm = doc.select("td[class=adv-text] > div[class$=b-media-cont_relative]").first();
        if (!SearchSettings.get(DromPage.SETTING_FUEL).get(0).isEmpty())
            Assert.assertTrue("Параметры дигателя указаны неверно!", elm.text().contains(SearchSettings.get(DromPage.SETTING_FUEL).get(0)));
        if (!SearchSettings.get(DromPage.SETTING_TRANSMISSION).get(0).isEmpty())
            Assert.assertTrue("Данные КПП указаны неверно!", elm.text().contains(SearchSettings.get(DromPage.SETTING_TRANSMISSION).get(0)));
        if (!SearchSettings.get(DromPage.SETTING_PRIVOD).get(0).isEmpty())
            Assert.assertTrue("Привод указаны неверно!", elm.text().contains(SearchSettings.get(DromPage.SETTING_PRIVOD).get(0)));
        if (!SearchSettings.get(DromPage.SETTING_WL).get(0).isEmpty()) {
            Assert.assertTrue("Руль указан неверно!", elm.text().contains(SearchSettings.get(DromPage.SETTING_WL).get(0).substring(0,
                    SearchSettings.get(DromPage.SETTING_WL).get(0).length() - 5)));
        }
        if (!SearchSettings.get(DromPage.SETTING_COLOR).get(0).isEmpty()) {
            ArrayList<String> colors = SearchSettings.get(DromPage.SETTING_COLOR);
            boolean result = false;
            for (String c : colors) {
                String[] colorElements = c.split("/");
                for (String color : colorElements) {
                    if (elm.text().contains(color))
                        result = true;
                }
            }
            Assert.assertTrue("Найден неверный цвет " + doc.baseUri(), result);
        }
        if (!SearchSettings.get(DromPage.SETTING_TRADE).get(0).isEmpty()) {
            elm = doc.select("td[class=adv-text] > p:contains(Обмен:)").last();
            ArrayList<String> sett = SearchSettings.get(DromPage.SETTING_TRADE);
            boolean reslut = false;
            for (String setting : sett) {
                String[] vl = elm.text().split(",");
                for (String v : vl) {
                    if (v.trim().contains(setting))
                        reslut = true;
                }
            }
            Assert.assertTrue("Неверные значения обмена!", reslut);
        }

        if (last_best_uri == null) {
            last_best_price = curr_price;
            last_best_year = curr_year;
            last_best_uri = curr_uri;
        } else {
            compareLastResult(curr_price, curr_year, curr_uri);
        }
        log("Машина[" + carNumber++ +"] " + curr_name + ": год=" + curr_year + ", цена: " + curr_price, curr_uri);
        System.out.println("OK!");
    }

    private void compareLastResult(float price, int year, String uri) {
        System.out.println("curr: " + price + " - " + year + " - " + uri);
        System.out.println("best: " + last_best_price + " - " + last_best_year + " - " + last_best_uri);
        if (year > last_best_year) {
            last_best_price = price;
            last_best_year = year;
            last_best_uri = uri;
            last_best_number = carNumber;
        }
        if (year == last_best_year) {
            if (price < last_best_price) {
                last_best_price = price;
                last_best_year = year;
                last_best_uri = uri;
                last_best_number = carNumber;
            }
        }
    }

    private void log(String text, String value) {
        Allure.LIFECYCLE.fire(new AddParameterEvent(text, value));
    }
}
