package ru.omen.app.stepDefinitions;

import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import lib.Init;
import org.openqa.selenium.WebDriver;
import ru.omen.app.pages.DromPage;
import ru.omen.app.pages.ResultPage;
import ru.yandex.qatools.allure.Allure;
import ru.yandex.qatools.allure.events.AddParameterEvent;

/**
 * Created by OmeN on 26.05.2016.
 */
public class CommonStepDefinitions {

    private WebDriver driver;
    private String url;
    private DromPage dromPage;
    private ResultPage resultPage;

    @Before
    public void setUp() {
        Init.initProperties();
        url = Init.getProperty("url").toString();
        this.driver = Init.getDriver();
    }

    @Given("^Открыть страницу для поиска$")
    public void open_url() {
        driver.get(url);
        Allure.LIFECYCLE.fire(new AddParameterEvent("Страница drom.ru", "Открыта"));
        Init.saveScreenshot();
    }

    @Then("^Страница открыта, переходим в раздел Автомобили$")
    public void load_page() {
        dromPage = new DromPage();
        dromPage.tabAuto();
        Allure.LIFECYCLE.fire(new AddParameterEvent("Вкладка Автомобили", "Открыта"));
        Init.saveScreenshot();
    }

    @Given("^Считываем файл с критериями отбора и устанавливаем их$")
    public void get_settings() throws InterruptedException {
        dromPage.setupSettings();
        Allure.LIFECYCLE.fire(new AddParameterEvent("Критерии", "Установлены"));
        Init.saveScreenshot();
    }

    @When("^Нажимаем кнопку поиска$")
    public void click_search() {
        dromPage.clickSearch();
        Allure.LIFECYCLE.fire(new AddParameterEvent("Кнопка 'Найти'", "Нажата"));
    }

    @Given("^Получаем страницу с результатом поиска$")
    public void load_page_result() {
        resultPage = new ResultPage();
        Allure.LIFECYCLE.fire(new AddParameterEvent("Результат поиска", "Получен"));
        Init.saveScreenshot();
    }

    @Given("^Проверяем результат поиска на соответствие критериям, находим нужный автомобиль$")
    public void parse_result() {
        resultPage.parseResult();
        Allure.LIFECYCLE.fire(new AddParameterEvent("Результат поиска", "Проверен"));
    }

    @Given("^Откроем страницу с найденным авто$")
    public void open_best_result() {
        resultPage.openBestResult();
        Allure.LIFECYCLE.fire(new AddParameterEvent("Найденная страница", "Открыта"));
        Init.saveScreenshot();
    }

    @After
    public void afterTest() {
        driver.quit();
        System.out.println("Completed!");
    }
}
