package lib;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import ru.omen.app.pages.DromPage;
import ru.yandex.qatools.allure.Allure;
import ru.yandex.qatools.allure.events.AddParameterEvent;

import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by OmeN on 28.05.2016.
 */
public class SearchSettings {

    public static enum Type {
        STR, INT
    }

    private static HashMap<String, ArrayList<String>> settings;

    private ArrayList<String> values;
    private String settName;
    private String settValue;

    public SearchSettings() {
        settings = new HashMap<String, ArrayList<String>>();
        String file = Init.getProperty("settings").toString();
        try {
            XSSFWorkbook myBook = new XSSFWorkbook(new FileInputStream(file));
            XSSFSheet mySheet = myBook.getSheetAt(0);
            for (int i = 0; i <= mySheet.getLastRowNum(); i++) {
                values = new ArrayList<>();
                settName = mySheet.getRow(i).getCell(0).getStringCellValue().toLowerCase().trim();

                try {
                    settValue = mySheet.getRow(i).getCell(1).getStringCellValue().toLowerCase().trim();
                } catch (NullPointerException e) {
                    e.printStackTrace();
                    settValue = "";
                }

                addSetting(DromPage.SETTING_FIRMA);
                addSetting(DromPage.SETTING_MODEL);
                addSetting(DromPage.SETTING_PRICE, "-", Type.INT);
                addSetting(DromPage.SETTING_YEAR, "-", Type.INT);
                addSetting(DromPage.SETTING_ENGINE, "-", Type.INT);
                addSetting(DromPage.SETTING_FUEL);
                addSetting(DromPage.SETTING_TRANSMISSION);
                addSetting(DromPage.SETTING_PRIVOD);
                addSetting(DromPage.SETTING_PHOTO);
                addSetting(DromPage.SETTING_WL);
                addSetting(DromPage.SETTING_INOMARKA);
                addSetting(DromPage.SETTING_COLOR, ",");
                addSetting(DromPage.SETTING_PTS);
                addSetting(DromPage.SETTING_STATE);
                addSetting(DromPage.SETTING_BODIES, ",");
                addSetting(DromPage.SETTING_TRADE, ",");
                addSetting(DromPage.SETTING_OTHER, ",");
            }
            System.out.println("==================================================");
        } catch (Throwable e) {
            e.printStackTrace();
        }

        for (Map.Entry entry : settings.entrySet()) {
            String k = entry.getKey().toString();
            for (String v : settings.get(k)) {
                System.out.println(k + " - " + v);
            }
        }
        System.out.println("==========================================");
    }

    public static ArrayList<String> get(String settName) {
        if (settings.get(settName) != null)
            return settings.get(settName);
        else
            return null;
    }

    private void addMultyValue(String v, String splitChar) {
        int settIndex = 0;
        String[] vls = v.split(splitChar);
        for (String s : vls)
            values.add(settIndex++, s.trim());
    }

    private void addMultyValue(String v, String splitChar, Type type) {
        int settIndex = 0;
        String[] vls = v.replace(" ", "").split(splitChar);
        for (String s : vls)
            values.add(settIndex++, s.trim());
    }

    private void addSetting(String name) {
        if (settName.contains(name)) {
            values.add(0, settValue.trim());
            settings.put(name, values);
            logSetting(name);
        }
    }

    private void addSetting(String name, String subChar) {
        if (settName.contains(name)) {
            addMultyValue(settValue, subChar);
            settings.put(name, values);
            logSetting(name);
        }
    }

    private void addSetting(String name, String subChar, Type type) {
        if (settName.contains(name)) {
            addMultyValue(settValue, subChar, type);
            settings.put(name, values);
            logSetting(name);
        }
    }

    public static boolean checkSetting(String name, String check_value, Type type) {
        ArrayList<String> values = settings.get(name);
        if (values.isEmpty())
            return false;
        switch (type) {
            case STR:
                for (String v : values) {
                    if (v.contains(check_value))
                        return true;
                }
                break;

            case INT:
                if (new Integer(check_value) >= new Integer(values.get(0)) && new Integer(check_value) <= new Integer(values.get(1))) {
                    return true;
                }
                break;
        }

        return false;
    }

    private void logSetting(String name) {
        Allure.LIFECYCLE.fire(new AddParameterEvent("Критерий '" + name + "'", values.toString()));
    }

    public static HashMap<String, ArrayList<String>> getSettings() {
        return settings;
    }
}
